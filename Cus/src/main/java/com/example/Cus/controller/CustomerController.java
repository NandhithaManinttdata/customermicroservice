package com.example.Cus.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.Cus.Exception.CollectionNotFoundException;
import com.example.Cus.Exception.DuplicateEntryException;
import com.example.Cus.Exception.RecordNotFoundException;
import com.example.Cus.model.Customer;
import com.example.Cus.service.CustomerService;

@RestController

public class CustomerController {
	private final Logger log=LoggerFactory.getLogger(CustomerController.class);

	
	@Autowired
	CustomerService service;

	@GetMapping("/Customers")

	public ResponseEntity getCustomers() {
		try
		{
			if(service.findAll().isEmpty()==false)
			return new ResponseEntity(service.findAll(), HttpStatus.OK);
			
			else
			{
				throw new CollectionNotFoundException("");
			}
		}
		catch(Exception e)
		{
			log.error("Customers List is empty "+ e);
			return new ResponseEntity("Customers List is empty", HttpStatus.OK);                 
		}
	}

	
	
	
	@DeleteMapping("/Customers/{cid}")

	public ResponseEntity  deleteCustomer(@PathVariable("cid") int cid) 
	{
		//
		try {
			 
			log.debug("Request{}",cid);
			Customer customer = service.getOne(cid);
			service.delete(customer);
			String response="Customer details got removed";
			log.debug("Response{}",response);
			
			return new ResponseEntity(response, HttpStatus.OK);// 200,404,405
			
		} catch (Exception e) {
			
			log.error("Customer entity not found "+e);
			
		return new ResponseEntity("Customer entity not found", HttpStatus.OK);// 200,404,405
			
		}
		
			
		
	}
			
		
		
			
		
	
		@GetMapping("/Customers/{cid}")

	public ResponseEntity getCustomer(@PathVariable("cid") int cid) 
	{
		try
		{
			log.debug("Request{}",cid);
			Optional<Customer> customer = service.findById(cid);
		    if (customer.isPresent()!=true)
               throw new RecordNotFoundException("object not found");
		    else
		    {
		    	String response="Customer details are";
		    	log.debug("Response{}",response);
		    	//log.info(response);
		    	return new ResponseEntity(customer, HttpStatus.NOT_FOUND);
		    }
		}
		
		
		catch(Exception e)
		{
			log.error("Customer entity not found "+e);
			return new ResponseEntity("entity not found", HttpStatus.NOT_FOUND);
		}
		
	}

	@PostMapping("/Customers")
	public ResponseEntity addCustomers(Customer customer) {
		try
		{  
			int i=customer.getCid();
		
			
		    
			Optional<Customer> customer1 = service.findById(i);
			
		     if(customer1.isPresent())
		     { 
		    	 throw new DuplicateEntryException("Object already exists"); 
		    	 
		     }
		    
		     else
		     {
		    	 System.out.println("new values");
		    	
		    	 service.save(customer);
		    	 return new ResponseEntity(customer, HttpStatus.OK); 
		     }
		
		}
		catch(Exception e)
		{
			log.error("customer object already exists "+e);
			return new ResponseEntity("customer object already exists", HttpStatus.NOT_FOUND);
			
		}

	}

	
	@PutMapping("/Customers")
	public ResponseEntity updateCustomers(@RequestBody Customer customer) 
	{
		
		try
		{
		 int i=customer.getCid();
		 Optional<Customer> customer1 = service.findById(i);
		 if(customer1.isPresent()!=false)
	     {  
	    	 
	    	 service.save(customer);
	    	 return new ResponseEntity(customer, HttpStatus.OK);
	    	 
	     }
	    
	     else
	     {
	    	 throw new RecordNotFoundException("");  
	    	  
	     }
	}
       catch(Exception e)
		{
    	   log.error("customer object doesnot exists "+e);
			return new ResponseEntity("customer object doesnt exists", HttpStatus.NOT_FOUND);
			
		}
       
       
		}



}
