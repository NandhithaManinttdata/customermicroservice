package com.example.Cus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CusApplication {

	public static void main(String[] args) {
		SpringApplication.run(CusApplication.class, args);
	}

}
