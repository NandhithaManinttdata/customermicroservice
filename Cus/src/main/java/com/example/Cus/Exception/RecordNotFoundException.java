package com.example.Cus.Exception;

public class RecordNotFoundException extends Exception {

	public RecordNotFoundException (String s)
	{
		super(s);
		System.out.println(s);
	}
	
}
