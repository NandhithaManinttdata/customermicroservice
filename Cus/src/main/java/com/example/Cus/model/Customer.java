package com.example.Cus.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Customer
{
	
	@Id
	private int cid;
	private String cname;
	private String cphone;
	
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getCphone() {
		return cphone;
	}
	public void setCphone(String cphone) {
		this.cphone = cphone;
	}
	
	
	

}
