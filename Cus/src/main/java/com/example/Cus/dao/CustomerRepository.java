package com.example.Cus.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.Cus.model.Customer;

public interface CustomerRepository  extends JpaRepository<Customer, Integer>

{
	 
	
}
