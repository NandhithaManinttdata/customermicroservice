package com.example.Cus.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.Cus.dao.CustomerRepository;
import com.example.Cus.model.Customer;

@Service
public class CustomerService 

{
	  @Autowired
	  CustomerRepository repo;
	public void savedetails(Customer cus) {
		repo.save(cus);
		
	}
	
	public void save(Customer cus) {
		repo.save(cus);
		
	}

	public Optional<Customer> findById(int cid) {
		Optional<Customer> cus=repo.findById(cid);
		return cus;
	}

	public List<Customer> findAll() {
		return (List<Customer>) repo.findAll();
	}

	
	public Customer getOne(int cid) {
		
		try {
		  Customer cus=repo.getOne(cid);
		  return cus;
		}
		catch(Exception e)
		{
			Customer c=null;
			System.out.println("Entity Not Found");
			return c ;
		}
	}
	public void delete(Customer cus) {
		repo.delete(cus);
		
	}

	

	
	
	
      	
	
}
